# Task Manager
## external repository
[jse-05](https://gitlab.com/projectnumber03/jse-05/)
## software
+ JRE
+ Java 8
+ Maven 4.0
## developer
Dmitry Shilov

email: [dunderflute@yandex.ru](mailto:dunderflute@yandex.ru)
## build application
```bash
mvn clean install
```
## run application
```bash
java -jar target/taskmanager-0.5.jar
```