package ru.shilov.tm.command.other;

import ru.shilov.tm.command.AbstractTerminalCommand;

public class ExitCommand extends AbstractTerminalCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Завершение работы";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
