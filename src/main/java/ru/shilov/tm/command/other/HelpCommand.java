package ru.shilov.tm.command.other;

import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.Comparator;
import java.util.stream.Collectors;

public class HelpCommand extends AbstractTerminalCommand {

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Список доступных комманд";
    }

    @Override
    public void execute() {
        String helpMessage = bootstrap.getCommands().values().stream()
                .sorted(Comparator.comparing(AbstractTerminalCommand::getName))
                .map(tc -> String.format("%s: %s", tc.getName(), tc.getDescription()))
                .collect(Collectors.joining("\n"));
        System.out.println(helpMessage);
    }

}
