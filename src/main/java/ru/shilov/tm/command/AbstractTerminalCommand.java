package ru.shilov.tm.command;

import ru.shilov.tm.context.Bootstrap;

public abstract class AbstractTerminalCommand {

    protected Bootstrap bootstrap;

    public abstract void execute() throws Exception;

    public abstract String getName();

    public abstract String getDescription();

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

}
