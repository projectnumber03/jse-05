package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ProjectFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        printAllProjectsWithTasks();
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Список проектов";
    }

    private void printAllProjectsWithTasks() throws NoSuchEntityException {
        List<Project> projects = bootstrap.getProjectService().findAll();
        for (int i = 1; i <= projects.size(); i++) {
            System.out.println(String.format("%d. %s", i, getProjectWithTasks(projects.get(i - 1).getId())));
        }
    }

    private String getProjectWithTasks(String id) throws NoSuchEntityException {
        List<Task> tasks = bootstrap.getTaskService().findAll().stream()
                .filter(t -> t.getIdProject().equals(id))
                .collect(Collectors.toList());
        return bootstrap.getProjectService().findOne(id).getName().concat("\n")
                .concat(IntStream.range(1, tasks.size() + 1).boxed()
                        .map(i -> String.format("\t%d. %s", i, tasks.get(i - 1).getName()))
                        .collect(Collectors.joining("\n")));
    }

}
