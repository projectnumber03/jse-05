package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;

public class ProjectRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        String idProject = bootstrap.getProjectService().getId(bootstrap.getTerminalService().nextLine());
        bootstrap.getProjectService().remove(idProject);
        System.out.println("[ПРОЕКТ УДАЛЕН]");
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Удаление проекта";
    }

}
