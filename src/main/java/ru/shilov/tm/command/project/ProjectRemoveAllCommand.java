package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;

public class ProjectRemoveAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        bootstrap.getProjectService().removeAll();
        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Удаление всех проектов";
    }

}
