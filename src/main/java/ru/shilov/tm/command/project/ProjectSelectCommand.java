package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;

public class ProjectSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        String idProject = bootstrap.getProjectService().getId(bootstrap.getTerminalService().nextLine());
        System.out.println(bootstrap.getProjectService().findOne(idProject).toString());
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "project-select";
    }

    @Override
    public String getDescription() {
        return "Свойства проекта";
    }

}
