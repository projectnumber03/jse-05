package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ProjectFindOneCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        String projectId = bootstrap.getProjectService().getId(bootstrap.getTerminalService().nextLine());
        System.out.println(getProjectWithTasks(projectId));
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "project-tasks";
    }

    @Override
    public String getDescription() {
        return "Список задач в проекте";
    }

    private String getProjectWithTasks(String id) throws NoSuchEntityException {
        List<Task> tasks = bootstrap.getTaskService().findAll().stream()
                .filter(t -> t.getIdProject().equals(id))
                .collect(Collectors.toList());
        return bootstrap.getProjectService().findOne(id).getName().concat("\n")
                .concat(IntStream.range(1, tasks.size() + 1).boxed()
                        .map(i -> String.format("\t%d. %s", i, tasks.get(i - 1).getName()))
                        .collect(Collectors.joining("\n")));
    }

}
