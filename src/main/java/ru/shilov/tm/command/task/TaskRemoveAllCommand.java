package ru.shilov.tm.command.task;

import ru.shilov.tm.command.AbstractTerminalCommand;

public class TaskRemoveAllCommand extends AbstractTerminalCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Удаление всех задач";
    }

    @Override
    public void execute() {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        bootstrap.getTaskService().removeAll();
        System.out.println("[ВСЕ ЗАДАЧИ УДАЛЕНЫ]");
    }

}
