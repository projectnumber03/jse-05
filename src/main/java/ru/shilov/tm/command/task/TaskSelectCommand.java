package ru.shilov.tm.command.task;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.error.PermissionException;

public class TaskSelectCommand extends AbstractTerminalCommand {

    @Override
    public String getName() {
        return "task-select";
    }

    @Override
    public String getDescription() {
        return "Свойства задачи";
    }

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        String taskId = bootstrap.getTaskService().getId(bootstrap.getTerminalService().nextLine());
        System.out.println(bootstrap.getTaskService().findOne(taskId).toString());
    }

}
