package ru.shilov.tm.command.task;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TaskFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println(getAllTasks());
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Список задач";
    }

    private String getAllTasks() {
        List<Task> tasks = bootstrap.getTaskService().findAll();
        return IntStream.range(1, tasks.size() + 1).boxed()
                .map(i -> (String.format("%d. %s", i, tasks.get(i - 1).getName())))
                .collect(Collectors.joining("\n"));
    }

}
