package ru.shilov.tm.command.task;

import ru.shilov.tm.command.AbstractTerminalCommand;

public class TaskRemoveCommand extends AbstractTerminalCommand {

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Удаление задачи";
    }

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        String taskId = bootstrap.getTaskService().getId(bootstrap.getTerminalService().nextLine());
        bootstrap.getTaskService().remove(taskId);
        System.out.println("[ЗАДАЧА УДАЛЕНА]");
    }

}
