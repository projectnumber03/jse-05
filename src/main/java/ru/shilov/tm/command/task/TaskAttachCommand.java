package ru.shilov.tm.command.task;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;

public class TaskAttachCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        String taskId = bootstrap.getTaskService().getId(bootstrap.getTerminalService().nextLine());
        Task t = bootstrap.getTaskService().findOne(taskId);
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        t.setIdProject(bootstrap.getProjectService().getId(bootstrap.getTerminalService().nextLine()));
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "task-attach";
    }

    @Override
    public String getDescription() {
        return "Добавление задачи в проект";
    }

}
