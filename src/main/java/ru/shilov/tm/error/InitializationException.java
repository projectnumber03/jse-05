package ru.shilov.tm.error;

public class InitializationException extends Exception {

    public InitializationException() {
        super("Ошибка инициализации");
    }

}
