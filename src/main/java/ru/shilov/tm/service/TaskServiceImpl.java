package ru.shilov.tm.service;

import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;

import java.util.List;
import java.util.UUID;

public class TaskServiceImpl implements IService<Task> {

    @Override
    public List<Task> findAll() {
        return taskRepo.findAll();
    }

    @Override
    public Task findOne(String id) throws NoSuchEntityException {
        Task task = taskRepo.findOne(id);
        if (isNullOrEmpty(id) || task == null) {
            throw new NoSuchEntityException();
        }
        return task;
    }

    @Override
    public void removeAll() {
        taskRepo.removeAll();
    }

    @Override
    public Task remove(String id) throws EntityRemoveException {
        if (isNullOrEmpty(id)) throw new EntityRemoveException();
        return taskRepo.remove(id);
    }

    @Override
    public Task persist(Task task) throws EntityPersistException {
        if (task == null) throw new EntityPersistException();
        task.setId(UUID.randomUUID().toString());
        return taskRepo.persist(task);
    }

    @Override
    public Task merge(Task task) throws EntityMergeException {
        if (task == null) throw new EntityMergeException();
        return taskRepo.merge(task);
    }

    @Override
    public String getId(String value) throws NumberToIdTransformException {
        if (!checkValue(value)) throw new NumberToIdTransformException(value);
        return taskRepo.getId(value);
    }

    private boolean checkValue(String value) {
        return !isNullOrEmpty(value) && value.matches("\\d+");
    }

}
