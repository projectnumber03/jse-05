package ru.shilov.tm.service;

import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ProjectServiceImpl implements IService<Project> {

    @Override
    public List<Project> findAll() {
        return projectRepo.findAll();
    }

    @Override
    public Project findOne(String id) throws NoSuchEntityException {
        Project project = projectRepo.findOne(id);
        if (isNullOrEmpty(id) || project == null) {
            throw new NoSuchEntityException();
        }
        return project;
    }

    @Override
    public void removeAll() {
        projectRepo.removeAll();
    }

    @Override
    public Project remove(String id) throws EntityRemoveException {
        if (isNullOrEmpty(id)) throw new EntityRemoveException();
        taskRepo.findAll().stream()
                .filter(t -> t.getIdProject().equals(id))
                .map(Task::getId)
                .collect(Collectors.toList())
                .forEach(taskRepo::remove);
        return projectRepo.remove(id);
    }

    @Override
    public Project persist(Project project) throws EntityPersistException {
        if (project == null) throw new EntityPersistException();
        project.setId(UUID.randomUUID().toString());
        return projectRepo.persist(project);
    }

    @Override
    public Project merge(Project project) throws EntityMergeException {
        if (project == null) throw new EntityMergeException();
        return projectRepo.merge(project);
    }

    @Override
    public String getId(String value) throws NumberToIdTransformException {
        if (!checkValue(value)) throw new NumberToIdTransformException(value);
        return projectRepo.getId(value);
    }

    private boolean checkValue(String value) {
        return !isNullOrEmpty(value) && value.matches("\\d+");
    }

}
