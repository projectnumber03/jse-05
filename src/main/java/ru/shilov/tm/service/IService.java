package ru.shilov.tm.service;

import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;
import ru.shilov.tm.repository.IRepository;
import ru.shilov.tm.repository.ProjectRepositoryImpl;
import ru.shilov.tm.repository.TaskRepositoryImpl;

import java.util.List;

public interface IService<T> {

    IRepository<Project> projectRepo = ProjectRepositoryImpl.getInstance();

    IRepository<Task> taskRepo = TaskRepositoryImpl.getInstance();

    List<T> findAll();

    T findOne(String id) throws NoSuchEntityException;

    void removeAll();

    T remove(String id) throws EntityRemoveException;

    T persist(T entity) throws EntityPersistException;

    T merge(T entity) throws EntityMergeException;

    String getId(String value) throws NumberToIdTransformException, NoSuchEntityException;

    default boolean isNullOrEmpty(String id) {
        return id == null || id.isEmpty();
    }

}
