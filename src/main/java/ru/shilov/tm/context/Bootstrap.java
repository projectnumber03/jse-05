package ru.shilov.tm.context;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.command.other.ExitCommand;
import ru.shilov.tm.command.other.HelpCommand;
import ru.shilov.tm.command.project.*;
import ru.shilov.tm.command.task.*;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.IllegalCommandException;
import ru.shilov.tm.service.IService;
import ru.shilov.tm.service.ProjectServiceImpl;
import ru.shilov.tm.service.TaskServiceImpl;
import ru.shilov.tm.service.TerminalService;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Bootstrap {

    public static DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private TerminalService terminalService = new TerminalService(new Scanner(System.in));

    private IService<Project> projectService = new ProjectServiceImpl();

    private IService<Task> taskService = new TaskServiceImpl();

    private Map<String, AbstractTerminalCommand> commands = initCommands();

    public void init() {
        System.out.println("*** ДОБРО ПОЖАЛОВАТЬ В ПЛАНИРОВЩИК ЗАДАЧ ***");
        while (true) {
            try {
                String command = terminalService.nextLine();
                if (!commands.containsKey(command)) throw new IllegalCommandException();
                commands.get(command).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, AbstractTerminalCommand> initCommands() {
        try {
            List<AbstractTerminalCommand> commands = Arrays.asList(
                    TaskFindAllCommand.class.getDeclaredConstructor().newInstance()
                    , ProjectFindAllCommand.class.getDeclaredConstructor().newInstance()
                    , ProjectFindOneCommand.class.getDeclaredConstructor().newInstance()
                    , TaskMergeCommand.class.getDeclaredConstructor().newInstance()
                    , ProjectMergeCommand.class.getDeclaredConstructor().newInstance()
                    , TaskPersistCommand.class.getDeclaredConstructor().newInstance()
                    , ProjectPersistCommand.class.getDeclaredConstructor().newInstance()
                    , TaskRemoveAllCommand.class.getDeclaredConstructor().newInstance()
                    , ProjectRemoveAllCommand.class.getDeclaredConstructor().newInstance()
                    , TaskRemoveCommand.class.getDeclaredConstructor().newInstance()
                    , ProjectRemoveCommand.class.getDeclaredConstructor().newInstance()
                    , TaskSelectCommand.class.getDeclaredConstructor().newInstance()
                    , ProjectSelectCommand.class.getDeclaredConstructor().newInstance()
                    , TaskAttachCommand.class.getDeclaredConstructor().newInstance()
                    , HelpCommand.class.getDeclaredConstructor().newInstance()
                    , ExitCommand.class.getDeclaredConstructor().newInstance()
            );
            commands.forEach(tc -> tc.setBootstrap(this));
            return commands.stream().collect(Collectors.toMap(tc -> Objects.requireNonNull(tc).getName(), tc -> tc));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    public IService<Project> getProjectService() {
        return projectService;
    }

    public IService<Task> getTaskService() {
        return taskService;
    }

    public TerminalService getTerminalService() {
        return terminalService;
    }

    public Map<String, AbstractTerminalCommand> getCommands() {
        return commands;
    }

}
