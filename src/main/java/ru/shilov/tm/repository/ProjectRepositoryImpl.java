package ru.shilov.tm.repository;

import ru.shilov.tm.entity.Project;
import java.util.*;
import java.util.stream.Collectors;

public class ProjectRepositoryImpl implements IRepository<Project> {

    private static IRepository<Project> instance = new ProjectRepositoryImpl();

    public static IRepository<Project> getInstance() {
        return instance;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    private ProjectRepositoryImpl() {
    }

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @Override
    public Project findOne(String id) {
        return findAll().stream().filter(p -> p.getId().equals(id)).findAny().orElse(null);
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

    @Override
    public Project remove(String id) {
        return projects.remove(id);
    }

    @Override
    public Project persist(Project project) {
        return projects.put(project.getId(), project);
    }

    @Override
    public Project merge(Project project) {
        return projects.put(project.getId(), new Project(project));
    }

    @Override
    public String getId(String value) {
        List<String> keys = new ArrayList<>(projects.keySet());
        return projects.size() >= Integer.parseInt(value) ? keys.get(Integer.parseInt(value) - 1) : "";
    }

}
