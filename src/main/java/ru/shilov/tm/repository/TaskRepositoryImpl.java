package ru.shilov.tm.repository;

import ru.shilov.tm.entity.Task;
import java.util.*;
import java.util.stream.Collectors;

public class TaskRepositoryImpl implements IRepository<Task> {

    private static IRepository<Task> instance = new TaskRepositoryImpl();

    public static IRepository<Task> getInstance() {
        return instance;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    private TaskRepositoryImpl() {
    }

    @Override
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @Override
    public Task findOne(String id) {
        return findAll().stream().filter(t -> t.getId().equals(id)).findAny().orElse(null);
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

    @Override
    public Task remove(String id) {
        return tasks.remove(id);
    }

    @Override
    public Task persist(Task task) {
        return tasks.put(task.getId(), task);
    }

    @Override
    public Task merge(Task task) {
        return tasks.put(task.getId(), new Task(task));
    }

    @Override
    public String getId(String value) {
        List<String> keys = new ArrayList<>(tasks.keySet());
        return tasks.size() >= Integer.parseInt(value) ? keys.get(Integer.parseInt(value) - 1) : "";
    }

}
