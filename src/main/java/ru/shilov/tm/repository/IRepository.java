package ru.shilov.tm.repository;

import java.util.List;

public interface IRepository<T> {

    List<T> findAll();

    T findOne(String id);

    void removeAll();

    T remove(String id);

    T persist(T entity);

    T merge(T entity);

    String getId(String value);

}
