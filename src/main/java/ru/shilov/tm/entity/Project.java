package ru.shilov.tm.entity;

import java.time.LocalDate;

import static ru.shilov.tm.context.Bootstrap.DATE_PATTERN;

public class Project extends AbstractEntity {

    private String name;

    private String description;

    private LocalDate start;

    private LocalDate finish;

    public Project() {
    }

    public Project(Project project) {
        super(project);
        this.name = project.name;
        this.description = project.description;
        this.start = project.start;
        this.finish = project.finish;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("проект: ").append(this.name).append("\n");
        sb.append("описание: ").append(this.description).append("\n");
        sb.append("дата начала: ").append(DATE_PATTERN.format(this.start)).append("\n");
        sb.append("дата окончания: ").append(DATE_PATTERN.format(this.finish));
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

}
