package ru.shilov.tm.entity;


public abstract class AbstractEntity {

    private String id;

    protected AbstractEntity() {
    }

    protected AbstractEntity(AbstractEntity entity) {
        this.id = entity.id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
